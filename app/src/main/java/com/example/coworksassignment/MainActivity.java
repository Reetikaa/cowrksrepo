package com.example.coworksassignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.coworksassignment.fragment.EventsFragment;
import com.example.coworksassignment.fragment.MyCenterFragment;
import com.example.coworksassignment.fragment.StreamFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final int TAB_SOCIAL = 0;
    private final int TAB_MESSAGING = 1;
    private final int TAB_BOOKING = 2, TAB_STORE =3 , TAB_HELP = 4;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private LinearLayout social, messaging, booking, help, store;
    private ImageView socialImage, messageImage, bookingImage, helpImage, storeImage;
    private TextView socialTextview, messageTex, bookingText, helpText, storeText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        viewPager =  findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout =  findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        social = findViewById(R.id.social);
        messaging =  findViewById(R.id.message);
        booking =  findViewById(R.id.booking);
        help =  findViewById(R.id.help);
        store =  findViewById(R.id.store);

        socialImage =  findViewById(R.id.socialImage);
        socialTextview =  findViewById(R.id.socialText);
        messageImage =  findViewById(R.id.messageImage);
        messageTex =  findViewById(R.id.messageText);
        bookingImage =  findViewById(R.id.bookingImage);
        bookingText =  findViewById(R.id.bookingText);
        helpImage =  findViewById(R.id.helpImage);
        helpText =  findViewById(R.id.helpText);
        storeImage =  findViewById(R.id.storeImage);
        storeText =  findViewById(R.id.storeText);

        social.setOnClickListener(this);
        messaging.setOnClickListener(this);
        booking.setOnClickListener(this);
        help.setOnClickListener(this);
        store.setOnClickListener(this);


    }

    //setting viewpager
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new StreamFragment(), "STREAM");
        adapter.addFragment(new EventsFragment(), "EVENTS");
        adapter.addFragment(new MyCenterFragment(), "MY CENTER");
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(viewPagerListener);
    }


    //setting listener for id
    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.social:
                viewPager.setCurrentItem(TAB_SOCIAL);

                break;
            case R.id.message:
                viewPager.setCurrentItem(TAB_MESSAGING);
                break;

            case R.id.booking:
                viewPager.setCurrentItem(TAB_BOOKING);

                break;
            case R.id.store:
                viewPager.setCurrentItem(TAB_STORE);
                break;

            case R.id.help:
                viewPager.setCurrentItem(TAB_HELP);
                break;

                
        }
    }


    //viewpager page change listener
    private ViewPager.OnPageChangeListener viewPagerListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            selectTab(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    //change of color on selected tab
    @SuppressLint("ResourceAsColor")
    private void selectTab(int tab){
        switch(tab){
            case TAB_SOCIAL :
                socialImage.setColorFilter(R.color.colorPrimary);
                socialTextview.setTextColor(Color.BLACK);
                messageImage.setColorFilter(Color.GRAY);
                messageTex.setTextColor(R.color.lightGrey);
                bookingImage.setColorFilter(Color.GRAY);
                bookingText.setTextColor(R.color.lightGrey);
                helpImage.setColorFilter(Color.GRAY);
                helpText.setTextColor(R.color.lightGrey);
                storeImage.setColorFilter(Color.GRAY);
                storeText.setTextColor(R.color.lightGrey);
                break;

            case TAB_MESSAGING :
                messageImage.setColorFilter(R.color.colorPrimary);
                messageTex.setTextColor(Color.BLACK);
                socialImage.setColorFilter(Color.GRAY);
                socialTextview.setTextColor(R.color.lightGrey);
                bookingImage.setColorFilter(Color.GRAY);
                bookingText.setTextColor(R.color.lightGrey);
                helpImage.setColorFilter(Color.GRAY);
                helpText.setTextColor(R.color.lightGrey);
                storeImage.setColorFilter(Color.GRAY);
                storeText.setTextColor(R.color.lightGrey);
                break;

            case TAB_BOOKING :
                bookingImage.setColorFilter(R.color.colorPrimary);
                bookingText.setTextColor(Color.BLACK);
                messageImage.setColorFilter(Color.GRAY);
                messageTex.setTextColor(R.color.lightGrey);
                socialImage.setColorFilter(Color.GRAY);
                socialTextview.setTextColor(R.color.lightGrey);
                helpImage.setColorFilter(Color.GRAY);
                helpText.setTextColor(R.color.lightGrey);
                storeImage.setColorFilter(Color.GRAY);
                storeText.setTextColor(R.color.lightGrey);
                break;

            case TAB_STORE :
                storeImage.setColorFilter(R.color.colorPrimary);
                storeText.setTextColor(Color.BLACK);
                socialImage.setColorFilter(Color.GRAY);
                socialTextview.setTextColor(R.color.lightGrey);
                messageImage.setColorFilter(Color.GRAY);
                messageTex.setTextColor(R.color.lightGrey);
                helpImage.setColorFilter(Color.GRAY);
                helpText.setTextColor(R.color.lightGrey);
                storeImage.setColorFilter(Color.GRAY);
                storeText.setTextColor(R.color.lightGrey);

                break;

            case TAB_HELP :
                helpImage.setColorFilter(R.color.colorPrimary);
                helpText.setTextColor(Color.BLACK);
                messageImage.setColorFilter(Color.GRAY);
                messageTex.setTextColor(R.color.lightGrey);
                socialImage.setColorFilter(Color.GRAY);
                socialTextview.setTextColor(R.color.lightGrey);
                storeImage.setColorFilter(Color.GRAY);
                storeText.setTextColor(R.color.lightGrey);
                storeImage.setColorFilter(Color.GRAY);
                storeText.setTextColor(R.color.lightGrey);
                break;


        }
    }

    //viewpager fragment class
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

