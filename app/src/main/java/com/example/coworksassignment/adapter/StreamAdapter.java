package com.example.coworksassignment.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.coworksassignment.R;
import com.example.coworksassignment.model.Datum;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class StreamAdapter extends RecyclerView.Adapter<StreamAdapter.MyViewHolder> {

    private final int HOUR_IN_MILLIS = 1000 * 60 * 60 * 60;
    private final int MULTIPLIER = 200;

    private List<Datum> articleList = new ArrayList<>(0);
    private Context context;
    private Calendar today = Calendar.getInstance();

    public StreamAdapter(List<Datum> articleList, Context context) {
        this.articleList = articleList;
        this.context = context;
    }

    //creating constructor
    public StreamAdapter(Context context) {
        this.context = context;
    }

    //updating the list
    public void updateList(List<Datum> list) {
        articleList.clear();
        articleList.addAll(list);
        notifyDataSetChanged();
    }

    //binding the view of item
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userName, description, companyName, time, location, likes;
        private CircleImageView userImage;

        public MyViewHolder(View view) {
            super(view);
            userName = view.findViewById(R.id.name);
            userImage = view.findViewById(R.id.profile_image);
            description = view.findViewById(R.id.description);
            companyName = view.findViewById(R.id.company_name);
            time = view.findViewById(R.id.time);
            location = view.findViewById(R.id.locations);
            likes = view.findViewById(R.id.likes);
        }
    }

    //inflating the layout
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stream_list_items, parent, false);

        return new MyViewHolder(itemView);
    }

    //binding the data to the item
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Datum article = articleList.get(position % articleList.size());
        holder.userName.setText(article.getCreator().getName());
        holder.companyName.setText(article.getCreator().getCompany());
        holder.description.setText((article.getArticle().getContent().toString()));
        holder.location.setText(article.getCreator().getCenter() + ", " + article.getCreator().getCity());

        if (article.getArticle().getLikes() != null && article.getArticle().getLikes().size() > 0 && !article.getArticle().getLikes().isEmpty()) {
            holder.likes.setText(article.getArticle().getLikes().get(0).getPkey() + " likes");


            long timeDifference = today.getTimeInMillis() - article.getArticle().getLikes().get(0).getTimelog();

            holder.time.setText((int) (timeDifference / HOUR_IN_MILLIS) + "h ago");
            Glide.with(context).load(article.getArticle().getLikes().get(0).getImageLoc()).into(holder.userImage);
        }


    }

    //getting list count
    @Override
    public int getItemCount() {
        return articleList.size() * MULTIPLIER;
    }

}