package com.example.coworksassignment.fragment;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.coworksassignment.R;
import com.example.coworksassignment.adapter.StreamAdapter;
import com.example.coworksassignment.model.Example;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * A simple {@link Fragment} subclass.
 */
public class StreamFragment extends Fragment {

    private RecyclerView recyclerView;
    private StreamAdapter adapter;
    private Gson gson;
    private Example example;

    //constructor
    public StreamFragment() {
        // Required empty public constructor
    }


    //inflating layout and initializing the items
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stream, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        adapter = new StreamAdapter(getActivity());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        //response of the local data and displaying them in list
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();

        try {
            StringBuilder strBuilder = myFunction(getContext());

            example = gson.fromJson(strBuilder.toString(), Example.class);

            adapter.updateList(example.getData());

        } catch (IOException e) {
            e.printStackTrace();
        }


        return view;
    }

    //reading data from local storage
    private StringBuilder myFunction(Context context) throws IOException {
        final Resources resources =  context.getResources();
        InputStream inputStream = resources.openRawResource(R.raw.response);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder strBuild = new StringBuilder();
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                strBuild.append(line);
            }
        } finally {
            reader.close();
        }
        return strBuild;

    }

}
