package com.example.coworksassignment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;



public class Article {

    @SerializedName("likes")
    @Expose
    private List<Like> likes = null;
    @SerializedName("comments")
    @Expose
    private List<Comment> comments = null;
    @SerializedName("image_loc")
    @Expose
    private String imageLoc;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("akey")
    @Expose
    private Long akey;
    @SerializedName("center_name")
    @Expose
    private String centerName;
    @SerializedName("center_id")
    @Expose
    private Long centerId;
    @SerializedName("timelog")
    @Expose
    private Long timelog;
    @SerializedName("content")
    @Expose
    private Object content;

    public List<Like> getLikes() {
        return likes;
    }

    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getImageLoc() {
        return imageLoc;
    }

    public void setImageLoc(String imageLoc) {
        this.imageLoc = imageLoc;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Long getAkey() {
        return akey;
    }

    public void setAkey(Long akey) {
        this.akey = akey;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public Long getCenterId() {
        return centerId;
    }

    public void setCenterId(Long centerId) {
        this.centerId = centerId;
    }

    public Long getTimelog() {
        return timelog;
    }

    public void setTimelog(Long timelog) {
        this.timelog = timelog;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

}