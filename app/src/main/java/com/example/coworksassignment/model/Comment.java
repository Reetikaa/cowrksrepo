package com.example.coworksassignment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {

    @SerializedName("image_loc")
    @Expose
    private String imageLoc;
    @SerializedName("ckey")
    @Expose
    private Long ckey;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("center")
    @Expose
    private String center;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("timelog")
    @Expose
    private Long timelog;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("pkey")
    @Expose
    private Long pkey;
    @SerializedName("content")
    @Expose
    private Object content;

    public String getImageLoc() {
        return imageLoc;
    }

    public void setImageLoc(String imageLoc) {
        this.imageLoc = imageLoc;
    }

    public Long getCkey() {
        return ckey;
    }

    public void setCkey(Long ckey) {
        this.ckey = ckey;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTimelog() {
        return timelog;
    }

    public void setTimelog(Long timelog) {
        this.timelog = timelog;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Long getPkey() {
        return pkey;
    }

    public void setPkey(Long pkey) {
        this.pkey = pkey;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

}